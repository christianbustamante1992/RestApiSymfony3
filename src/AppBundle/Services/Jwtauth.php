<?php 
namespace AppBundle\Services;

use Firebase\JWT\JWT;
/**
 * 
 */
class Jwtauth
{
	public $manager;
	public $key;

	public function __construct($manager)
	{
		$this->manager = $manager;
		$this->key = "masterpc1992";
	}

	public function signup($email,$contrasena,$gettoken = null)
	{
		$user = $this->manager->getRepository('BackendBundle:User')->findOneBy(array('email' => $email, 'password' => $contrasena));

		$respuesta = array('user' => null);

		if (is_object($user)) {

			$token = array(
				           'sub' => $user->getId(),
				           'email' => $user->getEmail(),
				           'name' => $user->getName(),
				           'surname' => $user->getSurname(),
				           'iat' => time(),
				           'exp' => time() + (7 * 24 * 60 * 60)

		                  );

			$jwt = JWT::encode($token,$this->key,'HS256');
			$decoded = JWT::decode($jwt,$this->key,array('HS256'));

			if ($gettoken === null) {
				$respuesta = $jwt;
			}else{
				$respuesta = $decoded;
			}

						
		}

		return $respuesta;
	}

	public function checkauth($jwt,$getIdentity = false)
	{
		$auth = false;

		try {

			$decoded = JWT::decode($jwt,$this->key,array('HS256'));
			
		} catch (\UnexpectedValueException $e) {

			$auth = false;
			
		} catch (\DomainException $e){

			$auth = false;

		}

		if (isset($decoded) && is_object($decoded) && isset($decoded->sub)) {

			$auth = true;

		}else{

			$auth = false;

		}

		if ($getIdentity == false) {
			return $auth;
		}else{
			return $decoded;
		}
	}
}

?>