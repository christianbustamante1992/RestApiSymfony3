<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Services\Helpers;
use AppBundle\Services\Jwtauth;

class DefaultController extends Controller
{
    
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    public function loginAction(Request $request)
    {
        $helpers = $this->get(Helpers::class);
        $jwt = $this->get(Jwtauth::class);
        $datarequest = $request->get('json',null);
        $dataresponse = array(
                              'status' => '500',
                              'response' => 'Peticion no valida'
                             );
        if ($datarequest != null) {
            //convertimos la data a un objeto
            $parametros = json_decode($datarequest);

            $email = (isset($parametros->email)) ? $parametros->email : null;
            $password = (isset($parametros->password)) ? $parametros->password : null;
            $gettoken = (isset($parametros->gettoken)) ? $parametros->gettoken : null;

            $validadoremail = new Assert\Email();
            $validadoremail->message = 'Email no valido';
            $validateemail = $this->get('validator')->validate($email,$validadoremail);

            

            if (count($validateemail) === 0 && $password != null) {

                if ($gettoken == null || $gettoken == false) {
                   $respuesta = $jwt->signup($email,$password);
                }else{
                    $respuesta = $jwt->signup($email,$password,true);
                }

                

                return $this->json($respuesta);
            }else{

                $dataresponse = array(
                              'status' => '500',
                              'response' => 'datos invalidos'
                             );

            }

            
        }

        return $helpers->getjson($dataresponse);


    }

    public function pruebaAction(Request $request)
    {
        $datarequest = $request->get('autorizacion',null);
        $helpers = $this->get(Helpers::class);
        $jwt = $this->get(Jwtauth::class);

        if ($datarequest && $jwt->checkauth($datarequest) == true) {
            $em = $this->getDoctrine()->getManager();
            $clase = $em->getRepository('BackendBundle:User');
            $users = $clase->findAll();

            
            return $helpers->getjson(array('status' => '200', 'response' => $users));
        }else{
            return $helpers->getjson(array('status' => '500', 'response' => 'Token no autorizado'));
        }
        
        
    }
}
